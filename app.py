from flask import Flask
import os

app = Flask(__name__)

@app.route('/hello/<string:name>')
def hello(name='world'):
    return 'Hello {}'.format(name)

@app.route('/')
def index():
    return "Index"

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=os.environ.get('PORT', 5000))
